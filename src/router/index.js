import Vue from 'vue'
import vantUI from 'vant-ui'
import VueRouter from 'vue-router'

import Home from '../views/Home.vue'

import Button from '../views/Button.vue'
import Cell from '../views/Cell.vue'
Vue.use(VueRouter)
Vue.use(vantUI)
const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/Cell',
    name: 'Cell',
    component: Cell
  },
  {
    path: '/Button',
    name: 'Button',
    component: Button
  }
]

const router = new VueRouter({
  routes
})

export default router
